# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''
:since: Oct 14, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory 
'''
import os

class ClusterConfig(object):
    """
    Interface definition for cluster configuration (mainly base modules for the moment)
    """
    
    def __init__(self):
        """
        Constructor
        """
        self._basemodules = None
        self._environmentcommands = []
        self._mainmodules = None
        self._batchcommand = ""
        self._mpicommand = ""
        self._pythoncommand = "python"
        self._noderequest = ""
        self._resourcerequest= ""
        self._corespernode = 0

    def GetListOfBaseModules(self):
        """
        Get the list of base module paths
        """
        return self._basemodules
    
    def GetEnvironmentCommands(self):
        return self._environmentcommands
    
    def GetListOfModsToLoad(self):
        """
        Get the list of main modules to be loaded
        """
        return self._mainmodules
    
    def _MakeDict(self):
        return {"modpaths":self._basemodules, "environ": self._environmentcommands, "mods": self._mainmodules, \
                 "batchcommand":self._batchcommand, "mpicommand":self._mpicommand, "pythoncommand": self._pythoncommand, \
                 "corespernode":self._corespernode}
    
    def _FromDict(self, mydict):
        self._basemodules = mydict["modpaths"]
        self._environmentcommands = mydict["environ"]
        self._mainmodules = mydict["mods"]
        self._batchcommand = mydict["batchcommand"]
        self._mpicommand = mydict["mpicommand"]
        self._pythoncommand = mydict["pythoncommand"]
        self._corespernode = mydict["corespernode"]
        
    def GetJobID(self):
        """
        Get JobID
        To be implemented for each cluster specially
        """
        pass
    
    def CreateSandboxLocation(self):
        pass
    
    def GetCoresPerNode(self):
        return self._corespernode
    
    def GetBatchCommand(self):
        return self._batchcommand
    
    def GetMPIcommand(self):
        return self._mpicommand
    
    def GetPythonCommand(self):
        return self._pythoncommand
    
    def GetResourceRequest(self):
        return self._resourcerequest

    def CalculateNumberOfMasterjobs(self, numberofjobs):
        """
        Calculate number of masterjobs needed for a requested split level on
        job counts
        """
        numberofmasters = int(numberofjobs) / int(self._corespernode)
        if numberofjobs % self._corespernode != 0:
            numberofmasters += 1
        return numberofmasters 
    
    def __getstate__(self):
        return self._MakeDict()
    
    def __setstate__(self, mydict):
        self._FromDict(mydict)

class HopperConfig(ClusterConfig):
    """
    Module and evironment configuration for the hopper cluster
    """
    
    def __init__(self):
        ClusterConfig.__init__(self)
        self._basemodules = ["/project/projectdirs/alice/hpc/modulefiles", "/opt/cray/gem/modulefiles", "/opt/cray/xt-asyncpe/default/modulefiles", \
                              "/opt/cray/modulefiles", "/opt/modulefiles", "/usr/common/usg/Modules/modulefiles", "/usr/syscom/nsg/modulefiles", \
                              "/usr/syscom/nsg/opt/modulefiles", "/usr/common/acts/Modules/modulefiles", "/usr/common/ftg/Modules/modulefiles", \
                              "/usr/common/graphics/Modules/modulefiles", "/usr/common/jgi/Modules/modulefiles", "/usr/common/tig/Modules/modulefiles", \
                              "/project/projectdirs/alice/hpc/hopper/modulefiles", "/project/projectdirs/alice/hpc/cvmfs/alice.cern.ch/x86_64-2.6-gnu-4.1.2/Modules/modulefiles"]
        self._environmentcommands = ["module swap PrgEnv-pgi PrgEnv-gnu"]
        self._mainmodules = ["ccm","openmpi-ccm","hopperlibs"]
        self._batchcommand = "qsub"
        self._mpicommand = "aprun -n"
        self._corespernode = 24
        self._resourcerequest = "-l mpplabels=bigmem"
        
    def GetJobID(self):
        """
        Get the Job ID
        """
        jstring = os.environ["PBS_JOBID"]
        return int(jstring.split(".")[0])
    
    def CreateSandboxLocation(self):
        """
        Make sandbox on scratch
        """
        return "%s/sim" %(os.getenv("SCRATCH"))
    
    def GetNodeRequest(self, numberofjobs):
        return "mppwidth=%d" %(numberofjobs)

    def __getstate__(self):
        return self._MakeDict()
    
    def __setstate__(self, mydict):
        self._FromDict(mydict)

class EdisonConfig(ClusterConfig):
    """
    Module and evironment configuration for the hopper cluster
    """
    
    def __init__(self):
        ClusterConfig.__init__(self)
        self._basemodules = ["/project/projectdirs/alice/hpc/modulefiles", "/opt/cray/gem/modulefiles", "/opt/cray/xt-asyncpe/default/modulefiles", \
                              "/opt/cray/modulefiles", "/opt/modulefiles", "/usr/common/usg/Modules/modulefiles", "/usr/syscom/nsg/modulefiles", \
                              "/usr/syscom/nsg/opt/modulefiles", "/usr/common/acts/Modules/modulefiles", "/usr/common/ftg/Modules/modulefiles", \
                              "/usr/common/graphics/Modules/modulefiles", "/usr/common/jgi/Modules/modulefiles", "/usr/common/tig/Modules/modulefiles"]
        self._environmentcommands = ["module swap PrgEnv-intel PrgEnv-gnu"]
        self._mainmodules = ["ccm","openmpi-ccm"]
        self._batchcommand = "qsub"
        self._mpicommand = "aprun -n"
        self._pythoncommand = "python-mpi"
        self._corespernode = 24         # Hyper-threading, but memory per node
        self._resourcerequest = ""
        
    def GetJobID(self):
        """
        Get the Job ID
        """
        jstring = os.environ["PBS_JOBID"]
        return int(jstring.split(".")[0])
    
    def CreateSandboxLocation(self):
        """
        Make sandbox on scratch
        """
        return "%s/sim" %(os.getenv("SCRATCH"))

    def GetNodeRequest(self, numberofjobs):
        return "mppwidth=%d" %(numberofjobs)

    def __getstate__(self):
        return self._MakeDict()
    
    def __setstate__(self, mydict):
        self._FromDict(mydict)
        
class CarverConfig(ClusterConfig):
    """
    Modules and environment for the charver configuration
    """
    
    def __init__(self):
        ClusterConfig.__init__(self)
        self._basemodules = ["/usr/share/Modules/modulefiles", "/usr/common/usg/Modules/modulefiles", "/usr/syscom/nsg/modulefiles", "/usr/syscom/nsg/opt/modulefiles", 
                              "/usr/common/acts/Modules/modulefiles", "/usr/common/ftg/Modules/modulefiles", "/usr/common/graphics/Modules/modulefiles", "/usr/common/jgi/Modules/modulefiles",
                              "/usr/common/tig/Modules/modulefiles", "/project/projectdirs/alice/hpc/cvmfs/alice.cern.ch/x86_64-2.6-gnu-4.1.2/Modules/modulefiles"]
        self._mainmodules = ["python","AliEn/v2-19-223"]
        self._environmentcommands = []
        self._batchcommand = "qsub"
        self._mpicommand = "mpirun -np"
        self._corespernode = 8
        self._resourcerequest = ""
    
    def GetJobID(self):
        jobstring = os.environ["PBS_JOBID"]
        return int(jobstring.split(".")[0])

    def CreateSandboxLocation(self):
        """
        Make sandbox on gscratch
        """
        return "%s/sim" %(os.getenv("GSCRATCH"))

    def GetNodeRequest(self, numberofjobs):
        numberofnodes = numberofjobs/8
        return "nodes=%d:ppn=8" %(numberofnodes)
    
class ClusterConfigHandler(object):
    """
    Handler for MPI cluster configurations
    """
    
    @staticmethod
    def GetConfigForCluster(clustername):
        if clustername == "Hopper":
            #print "Running jobs on Hopper"
            return HopperConfig()
        elif clustername == "Edison":
            #print "Running jobs on Edison"
            return EdisonConfig()
        elif clustername == "Carver":
            #print "Running jobs on Carver"
            return CarverConfig()
        else:
            print "Unknown cluster: %s" %(clustername)
            return None
