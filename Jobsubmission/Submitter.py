#! /usr/bin/env python
# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
Module running on the submitter side.
Parses the job definition and translates them into worker side job parameters,
which contain additional information
Furthermore it produces for the worker
- the master executable
- the local sandbox
The cluster is auto-detected from the name of the submitter host

:since: Oct 15, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
"""

import getopt, os, sys, shutil

import json as jsonhandler
from Jobsubmission.Jobparameters import Jobparams
from Jobsubmission.Sandbox import Sandbox
import subprocess
from Jobsubmission.ClusterConfig import ClusterConfigHandler
from Jobsubmission.OutputFileGroup import OutputFileGroup

class ProductionParameters(object):
    
    """
    Generalised set of parameters needed to produce the master executable.
    The parameters are mainly related to the batch system and deal with queue,
    time limit ...
    
    Getters  are self-explaining
    """
    
    def __init__(self):
        """
        Constructor
        """
        self.__jobname = "default"
        self.__nparallel = 0
        self.__pythonpath = None
        self.__globalsandbox = None
        self.__cluster = "Hopper"
        self.__timelimit = ""
        self.__queue = "regular"
        
    def SetTimeLimit(self, limit):
        self.__timelimit = limit
    
    def GetTimeLimit(self):
        return self.__timelimit
    
    def GetQueue(self):
        return self.__queue
    
    def SetQueue(self, queue):
        self.__queue = queue
        
    def SetCluster(self, cluster):
        self.__cluster = cluster

    def SetJobname(self, name):
        self.__jobname = name

    def GetJobname(self):
        return self.__jobname

    def SetNParallel(self, n):
        self.__nparallel = n
        
    def GetNParallel(self):
        return self.__nparallel

    def SetPYTHONPATH(self, path):
        self.__pythonpath = path

    def GetPYTHONPATH(self):
        return self.__pythonpath

    def SetGlobalsandbox(self, outdir):
        self.__globalsandbox = Sandbox(outdir)
        self.__globalsandbox.Create()

    def GetGlobalSandbox(self):
        return self.__globalsandbox
    
    def GetCluster(self):
        return self.__cluster
    
class ConfigWriterFactory(object):
    
    @staticmethod
    def GetConfigWriter(mode):
        if mode == "file":
            return FileConfigWriter()
        elif mode == "db":
            return DBConfigWriter()
            
class ConfigWriter(object):
    
    def __init__(self):
        self._entrylist = {}
        
    def AddObject(self, key, value, isPrimitive = False):
        self._entrylist[key] = value if isPrimitive else value.MakeDict()
    
class FileConfigWriter(ConfigWriter):
    
    def __init__(self):
        ConfigWriter.__init__(self)
    
    def Write(self, filename):
        configfile = open(filename, "w")
        configfile.write("%s\n" %(jsonhandler.dumps(self._entrylist)))
        configfile.close()
        
class PackageHandler(object):
    
    def __init__(self, sandbox):
        self.__sandbox = sandbox
        self.__packages = []
        if not os.path.exists(self.__sandbox):
            os.makedirs(self.__sandbox, 0755)
        # Create also module path
        moduledir = os.path.join(self.__sandbox, "modulefiles")
        if not os.path.exists(moduledir):
            os.makedirs(moduledir, 0755)
        self.InstallBaseModule()       
        
    def GetModulePath(self):
        return os.path.join(self.__sandbox, "modulefiles")
    
    def GetListOfPackages(self):
        return self.__packages
            
    def InstallBaseModule(self):
        template = "/project/projectdirs/alice/hpc/packages/modulefiles/templates/base_1.0"
        outputdir = os.path.join(self.__sandbox, "modulefiles", "BASE")
        if not os.path.exists(outputdir):
            os.makedirs(outputdir, 0755)
        
        templatereader = open(template)
        basewriter = open("%s/1.0" %outputdir, 'w')
        for line in templatereader:
            if "{BASEPATH}" in line:
                line = line.replace("{BASEPATH}", self.__sandbox)
            basewriter.write(line)
        templatereader.close()
        basewriter.close()
        self.__packages.append("BASE/1.0")
    
    def InstallPackage(self, packagename):
        print "Installing package %s" %packagename
        inputdir = "/project/projectdirs/alice/hpc/packages/%s" %(os.environ["NERSC_HOST"])
        packagetar = "%s.tar.gz" %(os.path.basename(packagename)) 
        packageinput = "%s/%s.tar.gz" %(inputdir, packagename)
        packagecategory = os.path.dirname(packagename)
        if os.path.exists(packageinput):
            packagedir = os.path.join(self.__sandbox, packagecategory)
            if not os.path.exists(packagedir):
                os.makedirs(packagedir, 0755)
            shutil.copy(packageinput, packagedir)
            pwd = os.getcwd()
            os.chdir(packagedir)
            os.system("tar xzf %s" %packagetar)
            os.remove(os.path.join(packagedir, packagetar))

            # install modulefile 
            modulecategory = os.path.dirname(packagename)
            moduleinput = "/project/projectdirs/alice/hpc/packages/modulefiles/%s/%s" %(os.environ["NERSC_HOST"], packagename)
            moduleout = os.path.join(self.__sandbox, "modulefiles", modulecategory)
            if not os.path.exists(moduleout):
                os.makedirs(moduleout, 0755)
            shutil.copy(moduleinput, moduleout)
            os.chdir(pwd)
            
            self.__packages.append(packagename)
    
class DBConfigWriter(ConfigWriter):
    
    def _init__(self):
        pass
    
    def Write(self):
        pass

class MasterJob(object):
    
    def __init__(self, params):
        self.__productionParams = params
        self.__jobparams = None
        self.__configdata = None
        self.__debugMode = False
        self.__jobid = 0
        
    def SetJobParams(self, params):
        self.__jobparams = params
        
    def SetDebugMode(self):
        self.__debugMode = True
        
    def WriteConfig(self, mode):
        writer = ConfigWriterFactory.GetConfigWriter(mode)
        writer.AddObject("jobparams", self.__jobparams)
        writer.AddObject("sandbox", self.__productionParams.GetGlobalSandbox().GetLocation(), isPrimitive = True)
        writer.AddObject("cluster", self.__productionParams.GetCluster(), isPrimitive = True)
        param = ""
        if mode == "file":
            param = "%s/jobconfig.json" %(self.__productionParams.GetGlobalSandbox().GetLocation())
        writer.Write(param)
        self.__configdata = {"mode":mode, "param":param}

    def GenerateSteeringScript(self):
        """
        Generate jobscript for MPI master
        """
        print "Generating MPI Wrapper for cluster %s" %(self.__productionParams.GetCluster())
        myclusterconf = ClusterConfigHandler.GetConfigForCluster(self.__productionParams.GetCluster()) 
        output = open("%s/master.sh" %(self.__productionParams.GetGlobalSandbox().GetLocation()), "w")
        output.write("#! /bin/bash -l\n")
        output.write("#PBS -N %s\n" %(self.__productionParams.GetJobname()))
        output.write("#PBS -q %s\n" %(self.__productionParams.GetQueue()))
        output.write("#PBS -l %s,walltime=%s\n" %(myclusterconf.GetNodeRequest(int(self.__productionParams.GetNParallel())), self.__productionParams.GetTimeLimit()))
        if len(myclusterconf.GetResourceRequest()):
            output.write("PBS %s\n" %(myclusterconf.GetResourceRequest()))
        output.write("#PBS -e %s/%s_$PBS_JOBID.err\n" %(self.__jobparams.GetOutputLocation(), self.__productionParams.GetJobname()))
        output.write("#PBS -o %s/%s_$PBS_JOBID.out\n" %(self.__jobparams.GetOutputLocation(), self.__productionParams.GetJobname()))
        
        output.write("export CRAY_ROOTFS=DSL\n")
        output.write("module load python\n")
        output.write("module load mpi4py\n")
        #set the pythonpath:
        output.write("export PYTHONPATH=$PYTHONPATH:%s\n" %(self.__productionParams.GetPYTHONPATH()))
        output.write("%s %d %s %s/Jobsubmission/Worker.py %s %s\n" %(myclusterconf.GetMPIcommand(), int(self.__productionParams.GetNParallel()), myclusterconf.GetPythonCommand(), \
                                                                 self.__productionParams.GetPYTHONPATH(), self.__configdata["mode"], self.__configdata["param"]))
        output.close()
        
    def Submit(self):
        executable = "%s %s/master.sh" %(ClusterConfigHandler.GetConfigForCluster(self.__productionParams.GetCluster()).GetBatchCommand(), self.__productionParams.GetGlobalSandbox().GetLocation())
        if self.__debugMode:
            print "Here I would do \' %s\'" %(executable) 
        else:
            submitproc = subprocess.Popen(executable, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
            for line in submitproc.stdout:
                self.__jobid = int(line.rstrip().split(".")[0])
                submitproc.stdout.flush()
                print "Job successfully submitted with ID %d" %(self.__jobid)
                
    def GetJobID(self):
        return self.__jobid
        
class JobDefinition(object):
    
    def __init__(self):
        self.__cluster = ""
        self.__executable = ""
        self.__executableArgs = ""
        self.__validation = ""
        self.__inputfiles = []
        self.__jobname = ""
        self.__nevents = 0
        self.__njobs = 0
        self.__nmasterjob = 1
        self.__modulepaths = []
        self.__modules = []
        self.__packages = []
        self.__outputbase = ""
        self.__outputfiles = []
        self.__timelimit = ""
        self.__queue = ""
        
    def ProcessConfigFile(self, filename):
        configfile = open(filename)
        for line in configfile:
            self.__HandleLine(line.replace("\n",""))
        configfile.close()
        
    def __HandleLine(self, line):
        tokens = line.split(":")
        key = tokens[0]
        value = tokens[1]
        if key == "executable":
            self.__executable = value.replace(" ", "")
        if key == "validation":
            self.__validation = value.replace(" ", "")
        elif key == "arguments":
            self.__executableArgs = value
        elif key == "outputbase":
            self.__outputbase = value.replace(" ", "")
        elif key == "jobname":
            self.__jobname = value.replace(" ", "")
        elif key == "njobs":
            self.__njobs = int(value.replace(" ", ""))
        elif key == "queue":
            self.__queue = value.replace(" ", "")
        elif key == "cluster":
            self.__cluster = value.replace(" ", "")
        elif key == "timelimit":
            self.__timelimit = value.replace(".",":").replace(" ", "")
        elif key == "modulepaths":
            for path in value.split(","):
                self.__modulepaths.append(path.replace(" ", ""))
        elif key == "modules":
            for mod in value.split(","):
                self.__modules.append(mod.replace(" ", ""))
        elif key == "packages":
            for pack in value.split(","):
                self.__packages.append(pack.replace(" ", ""))
        elif key == "inputfiles":
            for mac in value.split(","):
                self.__inputfiles.append(mac.replace(" ", ""))
        elif key == "outputfiles":
            for outf in value.split(";"):
                outf = outf.replace(" ", "")
                mygroup = OutputFileGroup()
                mygroup.BuildFromString(outf)
                self.__outputfiles.append(mygroup)
                
    def GetExecutable(self):
        return self.__executable
    
    def GetExecutableArgs(self):
        return self.__executableArgs
    
    def GetValidation(self):
        return self.__validation

    def GetOutputBase(self):
        return self.__outputbase
    
    def GetMacroPath(self):
        return self.__macrolocation
    
    def GetNumberOfEvents(self):
        return self.__nevents
    
    def GetNJobs(self):
        return self.__njobs
    
    def GetJobname(self):
        return self.__jobname
    
    def GetInputfiles(self):
        return self.__inputfiles
    
    def GetModulePaths(self):
        return self.__modulepaths
    
    def GetUserModules(self):
        return self.__modules
    
    def GetPackages(self):
        return self.__packages
    
    def GetCluster(self):
        return self.__cluster
    
    def GetQueue(self):
        return self.__queue
    
    def GetTimeLimit(self):
        return self.__timelimit
    
    def GetOutputfiles(self):
        return self.__outputfiles
    
def main():
    try:
        opt, arg = getopt.getopt(sys.argv[1:], "c:dr:")
    except getopt.GetoptError as e:
        print str(e)
        sys.exit(1)

    detectedCluster = ""
    nerscsystem = os.getenv("NERSC_HOST")
    if "hopper" in nerscsystem:
        detectedCluster = "Hopper"
    elif "edison" in nerscsystem:
        detectedCluster = "Edison"
    elif "carver" in nerscsystem:
        detectedCluster = "Carver"
    else:
        print "System unsupported"
        sys.exit(1)
    print "Submitting job for cluster %s" %(detectedCluster) 
        
    jobdef = JobDefinition()

    rundebug = False
    for o, a in opt:
        if o == "-c":
            jobdef.ProcessConfigFile(a)
        elif o == "-d":
            rundebug = True
            
    if len(jobdef.GetCluster()) and detectedCluster != jobdef.GetCluster():
        print "Cluster not matching the expectation: Selected %s, running %s" %(jobdef.GetCluster(), detectedCluster)
        sys.exit(1)
        
    # Create global output directory already at job submission level in order
    # to be able to write the logfile immediately there.
    if not os.path.exists(jobdef.GetOutputBase()):
        os.makedirs(jobdef.GetOutputBase(), 0755)
        
    # handle packages 
    globalsandbox = "%s/%s" %(ClusterConfigHandler.GetConfigForCluster(detectedCluster).CreateSandboxLocation(), jobdef.GetJobname())
    packagemodules = []
    packagemodulepaths = []
    if len(jobdef.GetPackages()):
        packman = PackageHandler("%s/packages" %globalsandbox)
        packagemodulepaths.append(packman.GetModulePath())
        for pack in jobdef.GetPackages():
            packman.InstallPackage(pack)
        for pack in packman.GetListOfPackages():
            packagemodules.append(pack)
    
    # handle pythonpath        
    scriptpath = os.path.realpath(os.path.dirname(sys.argv[0]))
    pythonpath = scriptpath.replace("/Jobsubmission", "")
            
    jobids = []
    nmaster = ClusterConfigHandler.GetConfigForCluster(detectedCluster).CalculateNumberOfMasterjobs(jobdef.GetNJobs())
    currentmin = 0
    for masterID in range(0, nmaster):
        jpar = Jobparams()
        ppar = ProductionParameters()
        ppar.SetCluster(detectedCluster)
        ppar.SetPYTHONPATH(pythonpath)

        for outputgroup in jobdef.GetOutputfiles():
            jpar.AddOutputGroup(outputgroup)
            
        sandboxdir = "%s/%s/master%d" %(ClusterConfigHandler.GetConfigForCluster(detectedCluster).CreateSandboxLocation(), jobdef.GetJobname(), masterID)
        print "Creating sandbox in %s" %(sandboxdir)
        if os.path.exists(sandboxdir):
            print "Output location already existing"
            sys.exit(1)
        ppar.SetGlobalsandbox(sandboxdir)
        if not ppar.GetGlobalSandbox().IsCreated():
            print "Global sandbox for the master job needs to be specified"
            sys.exit(1)
    
        jpar.SetOutputLocation(jobdef.GetOutputBase())
        jpar.SetExecutable(jobdef.GetExecutable())
        jpar.SetExecutableArgs(jobdef.GetExecutableArgs())
        jpar.SetValidation(jobdef.GetValidation())
        ppar.SetJobname(jobdef.GetJobname())

        # calculate number of workers per parallel job
        nparallel = ClusterConfigHandler.GetConfigForCluster(detectedCluster).GetCoresPerNode()
        if jobdef.GetNJobs() - currentmin < nparallel:
            nparallel = jobdef.GetNJobs() - currentmin
        ppar.SetNParallel(nparallel)
        ppar.SetQueue(jobdef.GetQueue() if len(jobdef.GetQueue()) else "regular")
        ppar.SetTimeLimit(jobdef.GetTimeLimit() if len(jobdef.GetTimeLimit()) else "12:00:00")

        if len(packagemodulepaths):
            for mod in packagemodulepaths:
                jpar.AddUserModulepath(mod)
        if len(packagemodules):
            for mod in packagemodules:
                jpar.LoadUserModule(mod)
        if len(jobdef.GetInputfiles()):
            for infi in jobdef.GetInputfiles():
                jpar.AddInputfile(infi)
        if len(jobdef.GetModulePaths()):
            for path in jobdef.GetModulePaths():
                jpar.AddUserModulepath(path)
        if len(jobdef.GetUserModules()):
            for mod in jobdef.GetUserModules():
                jpar.LoadUserModule(mod)
        
        mymaster = MasterJob(ppar)
        jpar.SetMasterJobID(masterID)
        jpar.SetGlobalJobRange(currentmin, currentmin + nparallel - 1)
        if rundebug:
            mymaster.SetDebugMode()
        mymaster.SetJobParams(jpar)
        mymaster.WriteConfig("file")
        mymaster.GenerateSteeringScript()
        mymaster.Submit()
        jobids.append(mymaster.GetJobID())
        currentmin += nparallel
        
    # Write all master job IDs to a file in the global sandbox
    jobidfile = open("%s/jobids" %globalsandbox, "w")
    for jobid in jobids:
        jobidfile.write("%d\n" %jobid)
    jobidfile.close()

    # Launch garbage collection
    os.system("%s/Jobsubmission/GarbageCollection.py --start --sandbox=%s --file=%s/jobids" %(pythonpath, globalsandbox, globalsandbox))

if __name__ == "__main__":
        main()
