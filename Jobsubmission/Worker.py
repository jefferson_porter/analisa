#! /usr/bin/env python
# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
This is the main module running on the worker. Depending on the job rank, it can be either a job 
steerer or a job launcher, while the job steerer contains a job launcher as well. The job steerer
creates the worker script and the local sandbox for each worker, and send the informations like 
job script and job params to the workers. The workers create the environment (modules), launch the 
job script, validate the job and send back the status and the execution time.

:since: Sept. 14, 2014 
:author: Markus Fasel 
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
"""

from mpi4py import MPI
import os, sys, time
import json as jsonhandler
from Jobsubmission.Jobparameters import Jobparams
from Jobsubmission.Sandbox import Sandbox
from Jobsubmission.ClusterConfig import ClusterConfigHandler
from shutil import rmtree as removedirectory
from fnmatch import fnmatch
from commands import getstatusoutput as runcommand
        
class CommunicationChannelHandler(object):
    """
    Handler for the communication channel
    """
    
    def __init__(self):
        pass

    @staticmethod
    def GetCommunicationChannel(channelname):
        channelID = 0
        if channelname == "params":
            channelID = 11
        elif channelname == "results":
            channelID = 12
        return channelID
        
class Jobscript(object):
    """
    Implementation of the job executable with special cluster and user configurations
    """
    
    def __init__(self, clusterconfig, jobparams, location, jobscriptname = "jobscript.sh"):
        """
        Constructor initalising values
        """
        self.__jobparams = jobparams
        self.__clusterconfig = clusterconfig
        self.__jobscriptname = jobscriptname
        self.__location = location
        
    def Create(self):
        """
        Create the jobscript
        """
        jobscript = open("%s/%s" %(self.__location, self.__jobscriptname), "w")
        jobscript.write("#!/bin/bash\n")
        jobscript.write("WD=$1\n")
        jobscript.write("JOBID=$2\n")
        jobscript.write("cd $WD\n")
        # Copy all macros to the sandbox
        for modpath in self.__clusterconfig.GetListOfBaseModules():
            jobscript.write("module use %s\n" %(modpath))
        # Add own module paths
        for modpath in self.__jobparams.GetUserModulePaths():
            jobscript.write("module use %s\n" %(modpath))
        for cmd in self.__clusterconfig.GetEnvironmentCommands():
            jobscript.write("%s\n" %(cmd))
        for mymod in self.__clusterconfig.GetListOfModsToLoad():
            jobscript.write("module load %s\n" %(mymod))
        # Load own modules
        for mymod in self.__jobparams.GetUserModules():
            jobscript.write("module load %s\n" %(mymod))
        jobscript.write("env\n");
        jobscript.write("module list\n")
        jobscript.write("cd $WD\n")
        jobscript.write("%s %s\n" %(self.__jobparams.GetExecutable(), self.__jobparams.GetExecutableArgs().replace("\"","").replace("#jobid", "$JOBID")))
        jobscript.close()
        os.chmod("%s/%s" %(self.__location, self.__jobscriptname), 0755)
        
    def Execute(self, sandbox, jobid):
        if sandbox.IsCreated():
            os.system("%s/%s %s %d &> %s/worker.log" %(self.__location, self.__jobscriptname, sandbox.GetLocation(), jobid, sandbox.GetLocation()))
            
    def MakeDict(self):
        return {"clusterconfig": self.__clusterconfig, "jobparams":self.__jobparams, "scriptname":self.__jobscriptname, "location":self.__location}
    
    def FromDict(self, mydict):
        """
        Reconstruct object from dictionary
        """
        self.__clusterconfig = mydict["clusterconfig"]
        self.__jobparams = mydict["jobparams"]
        self.__jobscriptname = mydict["scriptname"]
        self.__location = mydict["location"]
    
    def __getstate__(self):
        return self.MakeDict()

    def __setstate__(self, mydict):
        self.FromDict(mydict)
    
class WorkerParams():
    """
    Set of parameters (sim params, jobscript, local sandbox) the 
    master sends to the workers
    """
    
    def __init__(self, sandboxlocation, jobscript, jobparams, jobid):
        """
        Constructor
        """
        self.__sandbox = Sandbox(sandboxlocation)
        self.__jobscript = jobscript
        self.__jobparams = jobparams
        self.__jobid = jobid
        
    def GetJobscript(self):
        """
        Access to jobscript
        """
        return self.__jobscript
    
    def GetSandbox(self):
        """
        Access to sandbox location
        """
        return self.__sandbox
    
    def GetJobParams(self):
        """
        Access to job params
        """
        return self.__jobparams
    
    def GetJobID(self):
        """
        Return global job id for the worker
        """
        return self.__jobid
    
    def MakeDict(self):
        return {"jobscript":self.__jobscript, "sandbox":self.__sandbox, "jobparams": self.__jobparams, "jobid":self.__jobid}
    
    def FromDict(self, mydict):
        """
        Build jobparams object from dictionary (for serialisations)
        """
        self.__jobscript = mydict["jobscript"]
        self.__sandbox = mydict["sandbox"]
        self.__jobparams = mydict["jobparams"]
        self.__jobid = mydict["jobid"]
        
    def __getstate__(self):
        return self.MakeDict()
    
    def __setstate__(self, mydict):
        self.FromDict(mydict)
            
class WorkerResults(object):
    """
    Result structure the worker is sending back to the master
    """
    
    def __init__(self, jobstatus, exectime):
        """
        Constructor
        """
        self.__jobstatus = jobstatus
        self.__exectutiontime = exectime
        
    def SetJobstatus(self, jobstatus):
        """
        Set the jobstatus
        """
        self.__jobstatus = jobstatus
        
    def SetJobtime(self, exectime):
        """
        Set the execution time
        """
        self.__exectutiontime = exectime
        
    def GetJobtime(self):
        """
        Access to the execution time
        """
        return self.__exectutiontime
    
    def GetJobstatus(self):
        """
        Access to the job status
        """
        return self.__jobstatus
    
    def MakeDict(self):
        return {"jobstatus":self.__jobstatus, "executionTime":self.__exectutiontime}
    
    def FromDict(self, mydict):
        self.__jobstatus = mydict["jobstatus"]
        self.__exectutiontime= mydict["executionTime"]
        
    def __getstate__(self):
        return self.MakeDict()
    
    def __setstate__(self, mydict):
        self.FromDict(mydict)
              
class Jobsteerer(object):
    """
    Master job steering class handling different workers
    """ 
    
    def __init__(self, clusterConfig, globalSandbox, jobparams):
        """
        Constructor
        """
        self.__jobparams = jobparams
        self.__JobID = clusterConfig.GetJobID()
        self.__globalSandbox = globalSandbox
        self.__jobscript = Jobscript(clusterConfig, self.__jobparams, globalSandbox)
        self.__jobscript.Create()
        self.__workerresults = {}
        self.__starttime = time.time()
        self.__totaltime = 0
        self.__communicator = MPI.COMM_WORLD
        self.__localworker = Joblauncher()
    
    def SendParamsToWorkers(self):
        """
        Send the necessary parameters to the workers
        """
        for worker in range(1, self.__communicator.Get_size()):
            workerparams = WorkerParams("%s/job%d" %(self.__globalSandbox, worker), self.__jobscript, self.__jobparams, self.__jobparams.GetMinJobID() + worker)
            self.__SendToWorker(workerparams, worker)
        # Initialise local joblauncher as well
        self.__localworker.SetJobParams(WorkerParams("%s/job0" %(self.__globalSandbox), self.__jobscript, self.__jobparams, self.__jobparams.GetMinJobID()))
        
    def Execute(self):
        """
        Execute job on local worker
        """
        self.__localworker.Execute()
    
    def __SendToWorker(self, objects, workerID):
        """
        Send information to the worker
        """
        self.__communicator.send(objects, dest = workerID, tag = CommunicationChannelHandler.GetCommunicationChannel("params"))
    
    def HandleResults(self):
        """
        Receive results from the workers when finished
        Remove the global sandbox when the job is done
        """
        for worker in range(1, self.__communicator.Get_size()):
            self.__workerresults[worker] = self.__communicator.recv(source=worker, tag=CommunicationChannelHandler.GetCommunicationChannel("results")) 
        self.__workerresults[0] = self.__localworker.GetResults()
        self.__totaltime = time.time() - self.__starttime
        self.MoveOutput()
        self.WriteValidation()
        removedirectory(self.__globalSandbox)
        
    def MoveOutput(self):
        """
        Move files from the local sandbox to their final location
        After this is done, remove the local sandbox
        """
        for job in range(0, self.__communicator.Get_size()):
            localsandbox = Sandbox("%s/job%d" %(self.__globalSandbox, job))
            localsandbox.Refresh()
            outputdir = "%s/%04d" %(self.__jobparams.GetOutputLocation(), self.__jobparams.GetMinJobID() + job)
            #if self.__workerresults[job].GetJobstatus():
            print "moving results for jobs %d to its final location (%s)" %(job, outputdir)
            if not os.path.exists(outputdir):
                os.makedirs(outputdir, 0755)
            for mygroup in self.__jobparams.GetOutputGroups():
                #if not  self.__workerresults[job].GetJobstatus():
                #    print "Job %d failed" %(job)
                mygroup.WriteToFinalLocation(localsandbox, outputdir)
            removedirectory(localsandbox.GetLocation())
    
    def WriteValidation(self):
        """
        Write production summary
        """
        outputdir = "%s/meta%s" %(self.__jobparams.GetOutputLocation(), str(self.__JobID)) 
        os.makedirs(outputdir, 0755)
        valid = open("%s/outputs_valid" %(outputdir), 'w')
        valid.write("Job     Status     Time\n")
        valid.write("=============================\n")
        for j in sorted(self.__workerresults.keys()):
            valid.write("%d:\t%s\t%d\n" %(j, "Completed" if self.__workerresults[j].GetJobstatus() else "Failed", self.__workerresults[j].GetJobtime()))
        valid.write("Total time for the production: %d sec.\n" %(self.__totaltime))
        valid.close()
        
    
class Joblauncher(object):
    """
    Manager operating on the worker side
    """
    
    def __init__(self):
        self.__jobscript = None
        self.__jobparams = None
        self.__sandbox = None
        self.__jobid = 0
        self.__results = None
        self.__communicator = MPI.COMM_WORLD
        
    def ReceiveJobParams(self):
        """
        Receive necessary settings for the job execution (for MPI Communication)
        """
        workerParams = self.__communicator.recv(source = 0, tag = CommunicationChannelHandler.GetCommunicationChannel("params"))
        self.SetJobParams(workerParams)
        
    def SetJobParams(self, workerParams):
        """
        Set the worker params directly (for master job)
        """
        self.__sandbox = workerParams.GetSandbox()
        self.__jobid = workerParams.GetJobID()
        self.__jobscript = workerParams.GetJobscript()
        self.__jobparams = workerParams.GetJobParams()
       
    def Execute(self):
        """
        Execute the job
        
        Steps:
        1. Export global variables for the job (including seed)
        2. Create the sandbox
        3. Execute the jobscript
        4. Validate the output
        """
        if not self.__sandbox and self.__jobscript:
            print "Worker not properly initialised"
            return
        starttime = time.time()
        self.__jobparams.AddInputfilesToSandbox(self.__sandbox) # Add to the local sandbox
        self.__sandbox.Create()
        self.__sandbox.CopyToSandbox()
        os.chdir(self.__sandbox.GetLocation())
        self.__jobscript.Execute(self.__sandbox, self.__jobid)
        self.__results = WorkerResults(self.CheckForFiles() and self.ValidateJob(), time.time() - starttime)   
        self.CreateArchives()
        self.__sandbox.CleanSandbox()
        
    def CheckForFiles(self):
        """
        Check whether requested files are available
        Returns true if all matches are served, false otherwise
        """
        self.__sandbox.Refresh()
        status = True
        for mygroup in self.__jobparams.GetOutputGroups():
            if not mygroup.CheckSandboxForPatterns(self.__sandbox):
                status = False
        return status
                    
    def CreateArchives(self):
        """
        Create the output archives
        """
        for mygroup in self.__jobparams.GetOutputGroups():
            mygroup.CreateArchive(self.__sandbox)
        
    def ValidateJob(self):
        """
        Check if required files are there, and that there are no errors in the logfiles
        Also cleans up the sandbox
        """
        if self.__jobparams.HasValidation():
            result = runcommand(self.__jobparams.GetValidation())
            return result[0] 
        return True
    
    def HasMatch(self, filename, listofpatterns):
        """
        Check whether file is selected as output file using pattern matching with
        regular expressions (or just normal file names)
        """
        hasMatch = False
        for outputpattern in listofpatterns:
            if fnmatch(filename, outputpattern):
                hasMatch = True
        return hasMatch
    
    def CheckRootFile(self, filename):
        """
        Check whether requested file exists
        """
        return os.path.exists(filename)       
    
    def GetResults(self):
        """
        Get the results
        """
        return self.__results 
       
    def SendResults(self):
        """
        Send the status and the conoutation time back to the jobmaster
        """
        self.__communicator.send(self.__results, dest = 0, tag = CommunicationChannelHandler.GetCommunicationChannel("results"))
    
    def RemoveContent(self, path):
        """
        Remove all unwanted content
        """
        try:
            if os.path.isdir(path):
                removedirectory(path)
            else:
                os.remove(path)
        except Exception as e:
            print "Error in removing content %s: %s" %(path, str(e))

class ConfigHandlerFactory(object):
    
    @staticmethod
    def GetConfigHandler(mode, param):
        if mode == "file":
            return FileConfigHandler(param)
    
class ConfigHandler(object):
    """
    Base class for job configuration handling
    """
    
    def __init__(self):
        """
        Constructor
        """
        self._cluster = None
        self._jobparams = Jobparams()
        self._globalsandbox = None
        
    def GetCluster(self):
        """
        Get the cluster name
        """
        return self._cluster
        
    def GetGlobalSandbox(self):
        """
        Get global sandbox
        """
        return self._globalsandbox
        
    def GetJobparams(self):
        """
        Get general job params
        """
        return self._jobparams
    
class ConfigHandlerJSON(ConfigHandler):
    """
    Basic JSON configuration parser
    """
    
    def __init__(self):
        """
        Constructor, initialising configuration
        """
        ConfigHandler.__init__(self)
        
    def HandleJSONString(self, jsonstring):
        """
        Decode JSON string
        """
        objects = jsonhandler.loads(jsonstring)
        for coname,confobject in objects.iteritems():
            if coname == "cluster":
                self._cluster = str(confobject)
            elif coname == "sandbox":
                self._globalsandbox = Sandbox(str(confobject))
            elif coname == "jobparams":
                self._jobparams.InitialiseFromDict(confobject)
                    
class FileConfigHandler(ConfigHandlerJSON):
    
    def __init__(self, filename):
        ConfigHandlerJSON.__init__(self)
        self.ProcessConfigFile(filename)
    
    def ProcessConfigFile(self, filename):
        jsonstring = ""
        inputfile = open(filename, "r")
        for line in inputfile:
            jsonstring += line.replace("\n","")
        self.HandleJSONString(jsonstring)     
    
def IsMaster():
    """
    Define master job
    """
    return MPI.COMM_WORLD.Get_rank() == 0   

def main():
    """
    Main function handles worker tasks. If running on the master it also handles the Job steering
    """
      
    if IsMaster():
        jobconfig = ConfigHandlerFactory.GetConfigHandler(sys.argv[1], sys.argv[2])
        steering = Jobsteerer(ClusterConfigHandler.GetConfigForCluster(jobconfig.GetCluster()), jobconfig.GetGlobalSandbox().GetLocation(), jobconfig.GetJobparams())
        steering.SendParamsToWorkers()
        steering.Execute()  # run the local worker
        steering.HandleResults()
    else:
        worker = Joblauncher( )
        worker.ReceiveJobParams()
        worker.Execute()
        worker.SendResults()


if __name__ == "__main__":
        main()
