# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
:since: Nov 11, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
'''

import json as jsonhandler
import os
import shutil
from fnmatch import fnmatch

class Sandbox(object):
    """
    Generic worker sandbox
    """
    
    def __init__(self, location):
        """
        Constructor
        """
        self.__location = location
        self.__isCreated = os.path.exists(location)
        self.__inputobjects = []
        self.__content = []
        
    def GetLocation(self):
        """
        Get the sandbox location
        """
        return self.__location
    
    def IsCreated(self):
        """
        Check whether sandbox was already created on the worker
        """
        return self.__isCreated
    
    def Create(self):
        """
        Try to create the sandbox directory if not yet done
        """
        if not self.__isCreated:
            try:
                if not os.path.exists(self.__location):
                    os.makedirs(self.__location, 0755)
            except Exception as e:
                print "Failed creating the sandbox: %s" %(str(e))
                return
            self.__isCreated = True
        else:
            print "Sandbox already created"
            
    def AddInputObject(self, inputobject):
        """
        Add input object (inputfile) to sandbox
        """
        self.__inputobjects.append(inputobject)
        
    def CopyToSandbox(self):
        """
        Copy all inputfiles to the sandbox
        """
        if not self.__isCreated:
            print "Sandbox needs to be created before input objects can be copied"
        else:
            for inobj in self.__inputobjects:
                print "Copying %s to sandbox %s" %(inobj, self.__location)
                shutil.copy(inobj, self.__location)
    
    def CleanSandbox(self):
        """
        Remove all inputfiles from the sandbox after the job is done
        """
        for infi in self.__inputobjects:
            fname = os.path.basename(infi)
            if os.path.exists("%s/%s" %(self.__location, fname)):
                os.remove("%s/%s" %(self.__location, fname))
                
    def GetContent(self):
        """
        Return current state of the sandbox
        """
        return self.__content
    
    def GetFiles(self, filepattern):
        """
        Check for available files with a given pattern (can be a filename itself)
        Returns a list of matching files
        """
        result = []
        for currentfile in self.__content:
            if fnmatch(currentfile, filepattern):
                result.append(currentfile)
        return result

    def Refresh(self):
        """
        Refresh content of the sandbox
        """
        self.__content = os.listdir(self.GetLocation())
            
    def MakeDict(self):
        """
        Create dictionary for of the object
        """
        return {"location":self.__location, "isCreated": self.__isCreated, "inputobjects": self.__inputobjects, "content":self.__content}
    
    def FromDict(self, mydict):
        """
        Set the state from the dictionary
        """
        self.__location = mydict["location"]
        self.__isCreated = mydict["isCreated"]
        self.__inputobjects = mydict["inputobjects"]
    
    def __getstate__(self):
        return self.MakeDict()
    
    def __setstate__(self, mydict):
        self.FromDict(mydict) 
        
    def CreateJSONString(self):
        return jsonhandler.dumps(self.MakeDict())
